/**
 * Поддержка работы <input type='number'> в браузерах
 * @author chayn1k (pavelch@3davinci.ru)
 * @version 0.1 beta
 * @date 05.04.2012
 */

;(function($, undefined) {

	var inputNum = function(elem, conf) {
		this.namespace = 'inputNum';
		if ( this instanceof inputNum ) {
			return this.init(elem, conf);
		}
		return new inputNum(elem, conf);
	};

	inputNum.prototype = {

		init: function(elem, conf) {
			var self = this;

			self.conf = $.extend({ 
				cl_parent			: 'js-inum_parent',
				cl_container		: 'js-inum_wrap',
				cl_element			: 'js-inum-el',
				cl_buttonUp			: 'js-inum_up',
				cl_buttonDown		: 'js-inum_down', 
				cl_buttonUnactive	: 'unactive',
				delegateEvent 		: true,
				disableDefaultCtrl	: true
			}, conf);

			self.$elem = elem;
			//self.$inputs = self.$elem.find('.' + self.conf.cl_element);
			self.$supports = self._supportProps();

			// Делаем валидацию кнопок при изменении значения в поле
			self.$elem.on('change.inpNum', '.' + self.conf.cl_element, function (e) {
				self._buttValidity( $(this).parents('.' + self.conf.cl_container) );
			});


			self._setHandler();
			self._replaceInpNumber( self.$elem.find('.' + self.conf.cl_container) );

			self.$elem.find('.' + self.conf.cl_element).trigger('change.inpNum');

		
			return self;
		},

		_supportProps: function () {
			var props = 'autocomplete autofocus list placeholder max min multiple pattern required step valueAsNumber stepUp stepDown'.split(' ');

			var supp = {},
				attrs = {},
				inputElem = document.createElement('input');

			inputElem.setAttribute('type', 'number');
			supp['number'] = inputElem.type !== 'text';

			for ( var i = 0, len = props.length; i < len; i++ ) {
				attrs[ props[i] ] = !!(props[i] in inputElem);
			}
			if (attrs.list){
				// safari false positive's on datalist: webk.it/74252
				attrs.list = !!(document.createElement('datalist') && window.HTMLDataListElement);
			}
			supp['attrs'] = attrs;
			return supp;
		},

		_getElementProps: function ( inp ) {
			var self = this,
				min = parseInt(inp.attr('min'), 10) || 0, 
				max = parseInt(inp.attr('max'), 10) || false, 
				step = parseInt(inp.attr('step'), 10) || 1, 
				val = parseInt(inp.val(), 10);

			return {
				min: min, 
				max: max, 
				step: step, 
				val: val, 
				mismatch: (val - min) % step
			}
		},

		_stepUp: function ( inp ) {
			var self = this,
				conf = self.conf,
				props = self._getElementProps( inp );

			if ( !props.max || props.val + props.step <= props.max ) {
				return inp.val( props.val + props.step ).trigger('change');
			}
			return false;
		},

		_stepDown: function ( inp ) {
			var self = this,
				conf = self.conf,
				props = self._getElementProps( inp );

			if ( props.val - props.step >= props.min ) {
				return inp.val( props.val - props.step ).trigger('change');
			}
			return false;
		},

		_buttValidity: function ( context ) {
			var self = this,
				conf = self.conf,
				context = context || self.$elem,
				inp = context.find('.' + conf.cl_element),
				buttUp = context.find('.' + conf.cl_buttonUp),
				buttDown = context.find('.' + conf.cl_buttonDown);
				

			inp.each( function ( i, elem ) {
				var props = self._getElementProps( inp.eq(i) );
				if ( !props.max || props.val + props.step <= props.max ) {
					buttUp.eq(i).hasClass(conf.cl_buttonUnactive) && buttUp.eq(i).removeClass(conf.cl_buttonUnactive);
				} else {
					!buttUp.eq(i).hasClass(conf.cl_buttonUnactive) && buttUp.eq(i).addClass(conf.cl_buttonUnactive);
				}

				if ( props.val - props.step >= props.min ) {
					buttDown.eq(i).hasClass(conf.cl_buttonUnactive) && buttDown.eq(i).removeClass(conf.cl_buttonUnactive);
				} else {
					!buttDown.eq(i).hasClass(conf.cl_buttonUnactive) && buttDown.eq(i).addClass(conf.cl_buttonUnactive);
				}
			});
		},

		_changeVal: function ( e, method ) {
			var self = this,
				conf = self.conf,
				isEvent = $.type(e) === "object" && e instanceof jQuery.Event;

			if( isEvent ) {
				var butt = $(e.target),
					context = butt.parents('.' + conf.cl_container),
					inp = context.find('.' + conf.cl_element);

				butt.hasClass(conf.cl_buttonUp) ? self._stepUp( inp ) : self._stepDown( inp );
			} else if ( method == 'stepUp' || method == 'stepDown' ) {
				var inp = $(e),
					context = inp.parents('.' + conf.cl_container);

				method == 'stepUp' ? self._stepUp( inp ) : self._stepDown( inp );
			} else {
				return false;
			}
				
			//self._buttValidity( context );

			context.closest(self.$elem).trigger('change.inpNum');
		},

		_replaceInpNumber: function ( context ) {
			var self = this,
				conf = self.conf,
				context = context || self.$elem;
			if ( !self.$supports.number ) return false;
				
			if ( self.$supports.number && conf.disableDefaultCtrl ) {
				var inp = context.find('.' + conf.cl_element);

				inp.each( function (i, elem) {
					var _inp = $(this),
						_props = self._getElementProps( _inp );

					var newInput = _inp.clone(true).attr('type', 'text').data( _props );
					_inp.replaceWith(newInput);
				});
			}
			
			if ( self.$supports.number && !conf.disableDefaultCtrl ) {
				context.find('.' + conf.cl_buttonUp + ', .' + conf.cl_buttonDown).hide();
			};
		},

		_setHandler: function (  ) {
			var self = this,
				conf = self.conf;

			self.$elem.on('click.inpNum', '.' + conf.cl_buttonUp + ', .' + conf.cl_buttonDown , function (e) {
				self._changeVal( e );
			});

			self.$elem.on('keydown.inpNum', '.' + conf.cl_element, function (e) {
				var inp = $(this);

				if ( e.which == 38 ) self._changeVal(inp, 'stepUp');
				if ( e.which == 40 ) self._changeVal(inp, 'stepDown');
			});
			
			/*
			// TODO: доделать реакцию на изменение поля с клавиатуры
			self.$elem.on('keypress.inpNum', '.' + conf.cl_element, function (e) {
				self._buttValidity();
			});
			*/
		},

		_callAPI: function(api, arguments) {
			var self = this;
			if ( typeof self[api] !== 'function' ) {
				throw api + ' does not exist of inpNum methods.';
			} else if ( /^_/.test(api) && typeof self[api] === 'function' ) {
				throw 'Method begins with an underscore are not exposed.';
			}
			return self[api](arguments);
		},


// public
		stepUp: function() {
			var self = this;
			self._changeVal(self.$elem, 'stepUp');

			return self.$elem;
		},

		stepDown: function() {
			var self = this;
			self._changeVal(self.$elem, 'stepDown');

			return self.$elem;
		},

		setHandler: function ( callback ) {
			var self = this;
			self._setHandler(self.$elem);

			return self.$elem;
		},

		callbackChange: function ( callback ) {
			if ( !!callback && !$.isFunction(callback) ) return 0;
			var self = this;
			return self.$elem.on('change.inpNum', callback);
		}

		

	};


	// $.fn extend
	jQuery.fn.inpNum = function(conf, arguments) {
		var inpNum = this.data('inpNum');

		if ( inpNum ) {
			return inpNum._callAPI(conf, arguments);
		} else {
			inpNum = inputNum(this, conf);
			this.data('inpNum', inpNum);
			return this;
		}
	};

}(jQuery));


